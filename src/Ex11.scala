object Ex11 extends App {
  import Ex7.List
  import Ex7.List._

  def foldLeft[A](l : List[A])(acc : A)(map : (A,A) => A): A = l match {
    case Cons(h, t) => foldLeft(t)(map(acc,h))(map)
    case Nil() => acc
  }

  def foldRight[A] (l : List[A])(default : A)(f : (A,A) => A): A = l match {
    case Cons(h, t) => f(h,foldRight(t)(default)(f))
    case Nil() => default
  }

  val lst = Cons(3,Cons(7,Cons(1,Cons(5, Nil()))))

  println(foldLeft(lst)(0)(_-_))
  println(foldRight(lst)(0)(_-_))
}
