object Ex1 extends App  {
  //NEGATO
  val neg : (String => Boolean) => String => Boolean = f => v => !f(v)
  def neg (f : String => Boolean) : String => Boolean = v => !f(v)
  def neg (f : String => Boolean)(value : String) = !f(value)
}
