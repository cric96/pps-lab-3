
object Ex10 extends App {
  def genOp(f : (Int, Int) => Int, value : Int) : Int => Int = v => f(v, value)

  val incBy : Int => Int => Int = v => genOp(_+_, v)

  val plus7 = incBy(7)
  val minus3 = genOp(_-_, 3)
  val div2 = genOp(_/_, 2)
  import Ex4._
  println(compose(div2, minus3)(30))
  println(plus7(8))

}
