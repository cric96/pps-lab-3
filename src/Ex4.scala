object Ex4 extends App {
  def compose (f : Int => Int, g : Int => Int): Int => Int = value => f(g(value))
  println(compose(_ + 2, _ / 2)(5))
}
