object Ex6 extends App {
  sealed trait Person
  object Person{

    case class Student(name : String, year : Int) extends Person

    case class Teacher(name : String, course : String) extends Person

    def personToString(person : Person) : String = person match {
      case Student(name, year) => s"$name : $year"
      case Teacher(name, course) => name + s" [$course]"
    }
  }

  import Person._
  println(personToString(Student("Cricchio",2015)))
  println(personToString(Teacher("Viroli","OOP")))
}
