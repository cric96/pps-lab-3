object Ex5 extends App{

  def fibonacci (n : Int) : Int = n match {
    case 0 => 0
    case 1 => 1
    case n => fibonacci(n - 1) + fibonacci(n - 2)
  }

  def fibonacciTail (n : Int) : Int = {

    def _fib(n : Int, f : Int, s : Int) : Int = n match {
      case 2 => f + s
      case _ => _fib(n - 1 , f + s, f)
    }

    n match {
      case 0 => 0
      case 1 => 1
      case _ => _fib(n, 1,0)
    }
  }

}
