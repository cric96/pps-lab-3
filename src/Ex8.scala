

object Ex8 extends App {
  import Ex7._
  sealed trait Option[A]

  object Option {
    case class Some[A](element : A) extends Option[A]
    case class None[A]() extends Option[A]
    import List._

    def max(list :List[Int]) : Option[Int] = list match {
      case Cons(h, tail) => max(tail) match {
        case None() => Some(h)
        case Some(v) if h > v => Some(h)
        case Some(v) => Some(v)
      }
      case Nil() => None()
    }

    def maxTail(list : List[Int]) : Option[Int] = {

      def _max(list : List[Int], max : Int) : Int = list match {
        case Cons(h, t) if h > max => _max(t, h)
        case Cons(h, t) => _max(t, max)
        case _ => max
      }
      list match {
        case Nil() => None()
        case Cons(h, t) => Some(_max(t, h))
      }
    }

  }

  import Option._
  import Ex7.List._
  println(max(Cons(10, Cons(25, Cons(20, Nil())))))
  println(maxTail(Cons(10, Cons(25, Cons(20, Nil())))))
}
