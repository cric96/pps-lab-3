object Ex2 extends App {
  //PARITY
  val parity : Int => String = {
    case n if n % 2 == 0 => "even"
    case _ => "odd"
  }

  def parity(value : Int) = value match {
    case n if n % 2 == 0 => "even"
    case _ => "odd"
  }

}
