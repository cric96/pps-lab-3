import Ex6.Person
import Ex6.Person.{Student, Teacher}
object Ex9 extends App {
  import Ex7._
  import Ex7.List._
  def asCourse (persons: List[Person]) : List[String] = map (filter(persons)
    {
      case Teacher(_,_) => true;
      case _ => false
    }) { case Teacher(_, course) => course }

  println(asCourse(Cons(Teacher("Viroli", "PPS"), Cons(Student("Gianluca", 2018), Cons(Teacher("Casadei", "Concorrenti"),Nil())))))
}
