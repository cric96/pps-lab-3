object Ex3 extends App {
  val p11 : Int => Int => Int => Boolean = x => y => z => (x < y) && (y <= z )
  val p12 : Int => Int => Int => Boolean = x => y => z => (x < y) && (y <= z )

  val p13 : (Int, Int, Int) => Boolean = (x,y,z) => (x < y) && (y <= z)
  def p14(x : Int)(y : Int)(z : Int) = (x < y) && (y <= z)
  def p15(x : Int, y : Int, z : Int) = (x < y) && (y <= z)
}
