object Ex7 extends App {
  trait  List[E]
  object List {
    case class Cons[E](head: E, tail: List[E]) extends List[E]
    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case  Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def sumTail(l: List[Int]) : Int = {
      def _sum(l : List[Int], acc : Int) : Int = l match  {
        case  Cons(h, t) => _sum(t, acc + h)
        case _ => 0
      }
      sumTail(l)
    }
    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case (l1, Cons(h, t)) => Cons(h, append(l1, t))
      case _ => Nil()
    }

    def drop[A](l: List[A], n: Int): List[A] = l match {
      case Cons(_,t) if n > 0 => drop(t, n -1)
      case _ => l
    }

    def map[A,B](l: List[A])(f: A => B): List[B] = l match {
      case Cons(h, t) => Cons(f(h), map(t)(f))
      case _ => Nil()
    }

    def filter[A](l : List[A])(f : A => Boolean) : List[A] = l match {
      case Cons(h, t) if !f(h) => filter(t)(f)
      case Cons(h, t) => Cons(h, filter(t)(f))
      case _ => Nil()
    }
  }
  import List._

  println(drop(Cons(10, Cons(20, Cons(30, Nil()))),2))
  println(drop(Cons(10, Cons(20, Cons(30, Nil()))),5))
  println(map(Cons(10, Cons(20, Nil())))(_+1))
  println(map(Cons(10, Cons(20, Nil())))(":"+_+":"))
  println(filter(Cons(10, Cons(20, Nil())))(_>15))
  println(filter(Cons("a", Cons("bb", Cons("ccc", Nil()))))( _.length <=2))
}
